"use strict";
var RawMessage = (function () {
    function RawMessage(id, threadID, threadName, authorName, text, timestamp) {
        this.id = id;
        this.threadID = threadID;
        this.threadName = threadName;
        this.authorName = authorName;
        this.text = text;
        this.timestamp = timestamp;
    }
    return RawMessage;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = RawMessage;
//# sourceMappingURL=RawMessage.js.map