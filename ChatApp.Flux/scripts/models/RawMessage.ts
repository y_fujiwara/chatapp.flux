﻿export default class RawMessage {
    constructor(
        public id: string,
        public threadID: string,
        public threadName: string,
        public authorName: string,
        public text: string,
        public timestamp: number) {
    }
}