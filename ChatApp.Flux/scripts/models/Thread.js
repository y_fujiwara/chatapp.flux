"use strict";
var Thread = (function () {
    function Thread(id, name, lastMessage) {
        this.id = id;
        this.name = name;
        this.lastMessage = lastMessage;
    }
    return Thread;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Thread;
//# sourceMappingURL=Thread.js.map