"use strict";
var Message = (function () {
    function Message(id, threadID, authorName, date, text, isRead) {
        this.id = id;
        this.threadID = threadID;
        this.authorName = authorName;
        this.date = date;
        this.text = text;
        this.isRead = isRead;
    }
    return Message;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Message;
//# sourceMappingURL=Message.js.map