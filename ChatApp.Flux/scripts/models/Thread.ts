﻿import Message from "./Message";
export default class Thread {
    constructor(
        public id: string,
        public name: string,
        public lastMessage: Message) {
    }
}