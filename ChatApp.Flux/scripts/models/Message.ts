﻿export default class Message {
    constructor(
        public id: string,
        public threadID: string,
        public authorName: string,
        public date: Date,
        public text: string,
        public isRead: boolean) {
    }
}