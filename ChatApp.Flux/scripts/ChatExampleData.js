"use strict";
var RawMessage_1 = require('./models/RawMessage');
var ChatExampleData = (function () {
    function ChatExampleData() {
    }
    ChatExampleData.prototype.init = function () {
        localStorage.clear();
        localStorage.setItem('message', JSON.stringify([
            new RawMessage_1.default('m_ 1', 't_ 1', 'Jing and Bill', 'Bill', 'Hey Jing, want to give a Flux talk at ForwardJS?', Date.now() - 99999),
            new RawMessage_1.default('m_ 2', 't_ 1', 'Jing and Bill', 'Bill', 'Seems like a pretty cool conference.', Date.now() - 89999),
            new RawMessage_1.default('m_ 3', 't_ 1', 'Jing and Bill', 'Jing', 'Sounds good. Will they be serving dessert?', Date.now() - 79999),
            new RawMessage_1.default('m_ 4', 't_ 2', 'Dave and Bill', 'Bill', 'Hey Dave, want to get a beer after the conference?', Date.now() - 69999),
            new RawMessage_1.default('m_ 5', 't_ 2', 'Dave and Bill', 'Dave', 'Totally! Meet you at the hotel bar.', Date.now() - 59999),
            new RawMessage_1.default('m_ 6', 't_ 3', 'Functional Heads', 'Bill', 'Hey Brian, are you going to be talking about functional stuff?', Date.now() - 49999),
            new RawMessage_1.default(' m_ 7', 't_ 3', 'Bill and Brian', 'Brian', 'At ForwardJS? Yeah, of course. See you there!', Date.now() - 39999)
        ]));
    };
    return ChatExampleData;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ChatExampleData();
//# sourceMappingURL=ChatExampleData.js.map