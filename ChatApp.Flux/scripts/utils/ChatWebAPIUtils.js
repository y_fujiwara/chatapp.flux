"use strict";
var ChatServerActionCreators_1 = require('../actions/ChatServerActionCreators');
var RawMessage_1 = require('../models/RawMessage');
var ChatWebAPIUtils = (function () {
    function ChatWebAPIUtils() {
    }
    ChatWebAPIUtils.prototype.getAllMessages = function () {
        // simulate retreving data from a database
        var rawMessage = JSON.parse(localStorage.getItem('messages'));
        // simulate success callback
        ChatServerActionCreators_1.default.receiveAll(rawMessage);
    };
    ChatWebAPIUtils.prototype.createMessage = function (message, threadName) {
        // simulate writing to a database
        var rawMessages = JSON.parse(localStorage.getItem('messages'));
        var timestamp = Date.now();
        var id = 'm_' + timestamp;
        var threadID = message.threadID || ('t_' + Date.now());
        var createdMessage = new RawMessage_1.default(id, threadID, threadName, message.authorName, message.text, timestamp);
        rawMessages.push(createdMessage);
        localStorage.setItem('messages', JSON.stringify(rawMessages));
        // simulate success callback
        setTimeout(function () { return ChatServerActionCreators_1.default.receiveCreatedMessage(createdMessage); }, 0);
    };
    return ChatWebAPIUtils;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ChatWebAPIUtils();
//# sourceMappingURL=ChatWebAPIUtils.js.map