﻿import ChatServerActionCreators from '../actions/ChatServerActionCreators';
import Message from '../models/Message';
import RawMessage from '../models/RawMessage';

class ChatWebAPIUtils {
    getAllMessages() {
        // simulate retreving data from a database
        var rawMessage = JSON.parse(localStorage.getItem('messages'));
        // simulate success callback
        ChatServerActionCreators.receiveAll(rawMessage);
    }

    createMessage(message: Message, threadName: string) {
        // simulate writing to a database
        var rawMessages = JSON.parse(localStorage.getItem('messages')) as RawMessage[];
        var timestamp = Date.now();
        var id = 'm_' + timestamp;
        var threadID = message.threadID || ('t_' + Date.now());
        var createdMessage = new RawMessage(
            id,
            threadID, threadName,
            message.authorName,
            message.text,
            timestamp);
        rawMessages.push(createdMessage);
        localStorage.setItem('messages', JSON.stringify(rawMessages));

        // simulate success callback
        setTimeout(() => ChatServerActionCreators.receiveCreatedMessage(createdMessage), 0);
    }
}

export default new ChatWebAPIUtils();