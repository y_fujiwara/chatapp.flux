"use strict";
var Message_1 = require('../models/Message');
var ChatMessageUtils = (function () {
    function ChatMessageUtils() {
    }
    ChatMessageUtils.prototype.convertRawMessage = function (rawMessage, currentThreadID) {
        return new Message_1.default(rawMessage.id, rawMessage.threadID, rawMessage.authorName, new Date(rawMessage.timestamp), rawMessage.text, rawMessage.threadID === currentThreadID);
    };
    ChatMessageUtils.prototype.getCreatedMessageData = function (text, currentThreadID) {
        var timestamp = Date.now();
        return new Message_1.default('m_' + timestamp, currentThreadID, 'Bill', // hard coded for the example
        new Date(timestamp), text, true);
    };
    return ChatMessageUtils;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ChatMessageUtils();
//# sourceMappingURL=ChatMessageUtils.js.map