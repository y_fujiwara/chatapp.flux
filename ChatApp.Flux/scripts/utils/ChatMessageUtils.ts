﻿import Message from '../models/Message';
import RawMessage from '../models/RawMessage';

class ChatMessageUtils {
    convertRawMessage(rawMessage: RawMessage, currentThreadID: string) {
        return new Message(
            rawMessage.id,
            rawMessage.threadID,
            rawMessage.authorName,
            new Date(rawMessage.timestamp),
            rawMessage.text,
            rawMessage.threadID === currentThreadID
        );
    }
    getCreatedMessageData(text: string, currentThreadID: string) {
        var timestamp = Date.now();
        return new Message(
            'm_' + timestamp,
            currentThreadID,
            'Bill', // hard coded for the example
            new Date(timestamp),
            text,
            true
        );
    }
}

export default new ChatMessageUtils();