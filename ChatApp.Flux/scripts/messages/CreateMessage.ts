﻿export default class CreateMessage {
    constructor(public text: string, currentThreadID: string) { }
}