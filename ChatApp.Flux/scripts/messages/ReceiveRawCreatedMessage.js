"use strict";
var ReceiveRawCreatedMessage = (function () {
    function ReceiveRawCreatedMessage(rawMessage) {
        this.rawMessage = rawMessage;
    }
    return ReceiveRawCreatedMessage;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ReceiveRawCreatedMessage;
//# sourceMappingURL=ReceiveRawCreatedMessage.js.map