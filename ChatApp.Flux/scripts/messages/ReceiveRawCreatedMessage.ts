﻿import RawMessage from '../models/RawMessage';

export default class ReceiveRawCreatedMessage {
    constructor(public rawMessage: RawMessage) { }
}