﻿import RawMessage from '../models/RawMessage';

export default class ReceiveRawMessages {
    constructor(public rawMessages: RawMessage[]) { }
}