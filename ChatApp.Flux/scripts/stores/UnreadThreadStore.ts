﻿/// <reference path="../../typings/flux/flux.d.ts" />
import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import MessageStore from './MessageStore';
import ThreadStore from './ThreadStore';
import ClickThread from '../messages/ClickThread';
import ReceiveRawMessages from '../messages/ReceiveRawMessages';
import * as FluxUtils from 'flux/utils';
import Thread from '../models/Thread';

class UnreadThreadStore extends FluxUtils.Store<Thread> {
    getCount() {
        var threads = ThreadStore.getAll();
        var unreadCount = 0;
        for (var id in threads) {
            if (!threads[id].lastMessage.isRead) {
                unreadCount;
            }
        }
        return unreadCount;
    }

    __onDispatch(action: any) {
        ChatAppDispatcher.waitFor([ThreadStore.getDispatchToken(), MessageStore.getDispatchToken()]);
        if (action instanceof ClickThread) {
            this.__emitChange();
        }
        if (action instanceof ReceiveRawMessages) {
            this.__emitChange();
        }
    }
}

export default new UnreadThreadStore(ChatAppDispatcher);