/// <reference path="../../typings/flux/flux.d.ts" />
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ChatAppDispatcher_1 = require('../dispatcher/ChatAppDispatcher');
var ChatMessageUtils_1 = require('../utils/ChatMessageUtils');
var ThreadStore_1 = require('./ThreadStore');
var ClickThread_1 = require('../messages/ClickThread');
var CreateMessage_1 = require('../messages/CreateMessage');
var ReceiveRawMessages_1 = require('../messages/ReceiveRawMessages');
var FluxUtils = require('flux/utils');
var MessageStore = (function (_super) {
    __extends(MessageStore, _super);
    function MessageStore() {
        _super.apply(this, arguments);
        this.messages = {};
    }
    MessageStore.prototype.get = function (id) {
        return this.messages[id];
    };
    MessageStore.prototype.getAll = function () {
        return this.messages;
    };
    MessageStore.prototype.getAllForThread = function (threadID) {
        var threadMessages = new Array();
        for (var id in this.messages) {
            if (this.messages[id].threadID === threadID) {
                threadMessages.push(this.messages[id]);
            }
        }
        threadMessages.sort(function (a, b) {
            if (a.date < b.date) {
                return -1;
            }
            else if (a.date > b.date) {
                return 1;
            }
            return 0;
        });
        return threadMessages;
    };
    MessageStore.prototype.getAllForCurrentThread = function () {
        return this.getAllForThread(ThreadStore_1.default.getCurrentID());
    };
    MessageStore.prototype.addMessages = function (rawMessages) {
        var _this = this;
        rawMessages.forEach(function (message) {
            if (!_this.messages[message.id]) {
                _this.messages[message.id] = ChatMessageUtils_1.default.convertRawMessage(message, ThreadStore_1.default.getCurrentID());
            }
        });
    };
    MessageStore.prototype.markAllInThreadRead = function (threadID) {
        for (var id in this.messages) {
            if (this.messages[id].threadID === threadID) {
                this.messages[id].isRead = true;
            }
        }
    };
    MessageStore.prototype.__onDispatch = function (action) {
        if (action instanceof ClickThread_1.default) {
            ChatAppDispatcher_1.default.waitFor([ThreadStore_1.default.getDispatchToken()]);
            this.markAllInThreadRead(ThreadStore_1.default.getCurrentID());
            this.__emitChange();
        }
        if (action instanceof CreateMessage_1.default) {
            var message = ChatMessageUtils_1.default.getCreatedMessageData(action.text, ThreadStore_1.default.getCurrentID());
            this.messages[message.id] = message;
            this.__emitChange();
        }
        if (action instanceof ReceiveRawMessages_1.default) {
            this.addMessages(action.rawMessages);
            ChatAppDispatcher_1.default.waitFor([ThreadStore_1.default.getDispatchToken()]);
            this.markAllInThreadRead(ThreadStore_1.default.getCurrentID());
            this.__emitChange();
        }
    };
    return MessageStore;
}(FluxUtils.Store));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new MessageStore(ChatAppDispatcher_1.default);
//# sourceMappingURL=MessageStore.js.map