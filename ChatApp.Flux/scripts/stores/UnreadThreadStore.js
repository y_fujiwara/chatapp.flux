"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../typings/flux/flux.d.ts" />
var ChatAppDispatcher_1 = require('../dispatcher/ChatAppDispatcher');
var MessageStore_1 = require('./MessageStore');
var ThreadStore_1 = require('./ThreadStore');
var ClickThread_1 = require('../messages/ClickThread');
var ReceiveRawMessages_1 = require('../messages/ReceiveRawMessages');
var FluxUtils = require('flux/utils');
var UnreadThreadStore = (function (_super) {
    __extends(UnreadThreadStore, _super);
    function UnreadThreadStore() {
        _super.apply(this, arguments);
    }
    UnreadThreadStore.prototype.getCount = function () {
        var threads = ThreadStore_1.default.getAll();
        var unreadCount = 0;
        for (var id in threads) {
            if (!threads[id].lastMessage.isRead) {
                unreadCount;
            }
        }
        return unreadCount;
    };
    UnreadThreadStore.prototype.__onDispatch = function (action) {
        ChatAppDispatcher_1.default.waitFor([ThreadStore_1.default.getDispatchToken(), MessageStore_1.default.getDispatchToken()]);
        if (action instanceof ClickThread_1.default) {
            this.__emitChange();
        }
        if (action instanceof ReceiveRawMessages_1.default) {
            this.__emitChange();
        }
    };
    return UnreadThreadStore;
}(FluxUtils.Store));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new UnreadThreadStore(ChatAppDispatcher_1.default);
//# sourceMappingURL=UnreadThreadStore.js.map