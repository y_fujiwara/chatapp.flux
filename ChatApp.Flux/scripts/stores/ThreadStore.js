"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../typings/flux/flux.d.ts" />
var FluxUtils = require('flux/utils');
var ChatAppDispatcher_1 = require('../dispatcher/ChatAppDispatcher');
var ChatMessageUtils_1 = require('../utils/ChatMessageUtils');
var ClickThread_1 = require('../messages/ClickThread');
var ReceiveRawMessages_1 = require('../messages/ReceiveRawMessages');
var Thread_1 = require('../models/Thread');
var ThreadStore = (function (_super) {
    __extends(ThreadStore, _super);
    function ThreadStore() {
        _super.apply(this, arguments);
        this.currentID = null;
        this.threads = {};
    }
    ThreadStore.prototype.get = function (id) {
        return this.threads[id];
    };
    ThreadStore.prototype.getAll = function () {
        return this.threads;
    };
    ThreadStore.prototype.getCurrentID = function () {
        return this.currentID;
    };
    ThreadStore.prototype.getCurrent = function () {
        return this.get(this.getCurrentID());
    };
    ThreadStore.prototype.init = function (rawMessages) {
        var _this = this;
        rawMessages.forEach(function (message) {
            var threadID = message.threadID;
            var thread = _this.threads[threadID];
            if (thread && thread.lastMessage.date.getTime() > message.timestamp) {
                return;
            }
            _this.threads[threadID] = new Thread_1.default(threadID, message.threadName, ChatMessageUtils_1.default.convertRawMessage(message, _this.currentID));
        });
        if (!this.currentID) {
            var allChrono = this.getAllChrono();
            this.currentID = allChrono[allChrono.length - 1].id;
        }
        this.threads[this.currentID].lastMessage.isRead = true;
    };
    ThreadStore.prototype.getAllChrono = function () {
        var orderedThreads = new Array();
        for (var id in this.threads) {
            var thread = this.threads[id];
            orderedThreads.push(thread);
        }
        orderedThreads.sort(function (a, b) {
            if (a.lastMessage.date < b.lastMessage.date) {
                return -1;
            }
            else if (a.lastMessage.date > b.lastMessage.date) {
                return 1;
            }
            return 0;
        });
        return orderedThreads;
    };
    ThreadStore.prototype.__onDispatch = function (action) {
        if (action instanceof ClickThread_1.default) {
            var x = action;
            this.currentID = x.threadID;
            this.threads[this.currentID].lastMessage.isRead = true;
            this.__emitChange();
        }
        if (action instanceof ReceiveRawMessages_1.default) {
            this.init(action.rawMessages);
            this.__emitChange();
        }
    };
    return ThreadStore;
}(FluxUtils.Store));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ThreadStore(ChatAppDispatcher_1.default);
//# sourceMappingURL=ThreadStore.js.map