﻿/// <reference path="../../typings/flux/flux.d.ts" />
import * as FluxUtils from 'flux/utils';
import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import ChatMessageUtils from '../utils/ChatMessageUtils';
import ClickThread from '../messages/ClickThread';
import ReceiveRawMessages from '../messages/ReceiveRawMessages';
import RawMessage from '../models/RawMessage';
import Thread from '../models/Thread';

class ThreadStore extends FluxUtils.Store<Thread> {
    private currentID: string = null;
    private threads: { [key: string]: Thread } = {};

    get(id: string) {
        return this.threads[id];
    }

    getAll() {
        return this.threads;
    }

    getCurrentID() {
        return this.currentID;
    }

    getCurrent() {
        return this.get(this.getCurrentID());
    }

    init(rawMessages: RawMessage[]) {
        rawMessages.forEach(message => {
            var threadID = message.threadID;
            var thread = this.threads[threadID];
            if (thread && thread.lastMessage.date.getTime() > message.timestamp) {
                return;
            }
            this.threads[threadID] = new Thread(
                threadID,
                message.threadName,
                ChatMessageUtils.convertRawMessage(message, this.currentID));
        });

        if (!this.currentID) {
            var allChrono = this.getAllChrono();
            this.currentID = allChrono[allChrono.length - 1].id;
        }

        this.threads[this.currentID].lastMessage.isRead = true;
    }
    getAllChrono() {
        var orderedThreads = new Array<Thread>();
        for (var id in this.threads) {
            var thread = this.threads[id];
            orderedThreads.push(thread);
        }
        orderedThreads.sort((a, b) => {
            if (a.lastMessage.date < b.lastMessage.date) {
                return -1;
            } else if (a.lastMessage.date > b.lastMessage.date) {
                return 1;
            }
            return 0;
        });
        return orderedThreads;
    }

    __onDispatch(action: any) {
        if (action instanceof ClickThread) {
            var x = action as ClickThread;
            this.currentID = x.threadID;
            this.threads[this.currentID].lastMessage.isRead = true;
            this.__emitChange();
        }
        if (action instanceof ReceiveRawMessages) {
            this.init((action as ReceiveRawMessages).rawMessages);
            this.__emitChange();
        }
    }
}

export default new ThreadStore(ChatAppDispatcher);