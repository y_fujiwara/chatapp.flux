﻿/// <reference path="../../typings/flux/flux.d.ts" />

import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import ChatMessageUtils from '../utils/ChatMessageUtils';
import ThreadStore from './ThreadStore';
import ClickThread from '../messages/ClickThread';
import CreateMessage from '../messages/CreateMessage';
import ReceiveRawMessages from '../messages/ReceiveRawMessages';
import Message from '../models/Message';
import RawMessage from '../models/RawMessage';
import * as FluxUtils from 'flux/utils';

class MessageStore extends FluxUtils.Store<Message> {
    private messages: { [key: string]: Message } = {};

    get(id: string) {
        return this.messages[id];
    }

    getAll() {
        return this.messages;
    }

    getAllForThread(threadID: string) {
        var threadMessages = new Array<Message>();
        for (var id in this.messages) {
            if (this.messages[id].threadID === threadID) {
                threadMessages.push(this.messages[id]);
            }
        }
        threadMessages.sort((a, b) => {
            if (a.date < b.date) { return -1; }
            else if (a.date > b.date) { return 1; }
            return 0;
        });
        return threadMessages;
    }

    getAllForCurrentThread() {
        return this.getAllForThread(ThreadStore.getCurrentID());
    }

    private addMessages(rawMessages: RawMessage[]) {
        rawMessages.forEach(message => {
            if (!this.messages[message.id]) {
                this.messages[message.id] = ChatMessageUtils.convertRawMessage(message, ThreadStore.getCurrentID());
            }
        });
    }

    private markAllInThreadRead(threadID: string) {
        for (var id in this.messages) {
            if (this.messages[id].threadID === threadID) {
                this.messages[id].isRead = true;
            }
        }
    }

    __onDispatch(action: any) {
        if (action instanceof ClickThread) {
            ChatAppDispatcher.waitFor([ThreadStore.getDispatchToken()]);
            this.markAllInThreadRead(ThreadStore.getCurrentID());
            this.__emitChange();
        }
        if (action instanceof CreateMessage) {
            var message = ChatMessageUtils.getCreatedMessageData((action as CreateMessage).text, ThreadStore.getCurrentID());
            this.messages[message.id] = message;
            this.__emitChange();
        }
        if (action instanceof ReceiveRawMessages) {
            this.addMessages((action as ReceiveRawMessages).rawMessages);
            ChatAppDispatcher.waitFor([ThreadStore.getDispatchToken()]);
            this.markAllInThreadRead(ThreadStore.getCurrentID());
            this.__emitChange();
        }
    }
}

export default new MessageStore(ChatAppDispatcher);
