﻿/// <reference path="../typings/react/react.d.ts" />
/// <reference path="../typings/react/react-dom.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
import * as React from 'react';
import * as ReactDom from 'react-dom';

class App extends React.Component<{}, {}> {
    render() {
        return (
            <h1>
                Hello wor
            </h1>
        );
    }
}

ReactDom.render(
    <App />,
    document.getElementById('content'));