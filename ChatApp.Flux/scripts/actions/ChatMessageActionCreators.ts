﻿import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import CreateMessage from '../messages/CreateMessage';
import ChatWebAPIUtils from '../utils/ChatWebAPIUtils';
import ChatMessageUtils from '../utils/ChatMessageUtils';

class ChatMessageActionCreators {
    createMessage(text: string, currentThreadID: string) {
        ChatAppDispatcher.dispatch(new CreateMessage(text, currentThreadID));
        var message = ChatMessageUtils.getCreatedMessageData(text, currentThreadID);
        ChatWebAPIUtils.createMessage(message, null);
    }
}

export default new ChatMessageActionCreators();