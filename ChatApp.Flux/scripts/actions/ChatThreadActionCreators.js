"use strict";
var ChatAppDispatcher_1 = require('../dispatcher/ChatAppDispatcher');
var ClickThread_1 = require('../messages/ClickThread');
var ChatThreadActionCreators = (function () {
    function ChatThreadActionCreators() {
    }
    ChatThreadActionCreators.prototype.clickThread = function (threadID) {
        ChatAppDispatcher_1.default.dispatch(new ClickThread_1.default(threadID));
    };
    return ChatThreadActionCreators;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ChatThreadActionCreators();
//# sourceMappingURL=ChatThreadActionCreators.js.map