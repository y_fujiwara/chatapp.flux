"use strict";
var ChatAppDispatcher_1 = require('../dispatcher/ChatAppDispatcher');
var ReceiveRawMessages_1 = require('../messages/ReceiveRawMessages');
var ReceiveRawCreatedMessage_1 = require('../messages/ReceiveRawCreatedMessage');
var ChatServerActionCreators = (function () {
    function ChatServerActionCreators() {
    }
    ChatServerActionCreators.prototype.receiveAll = function (rawMessagesp) {
        ChatAppDispatcher_1.default.dispatch(new ReceiveRawMessages_1.default(rawMessagesp));
    };
    ChatServerActionCreators.prototype.receiveCreatedMessage = function (createdMessage) {
        ChatAppDispatcher_1.default.dispatch(new ReceiveRawCreatedMessage_1.default(createdMessage));
    };
    return ChatServerActionCreators;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ChatServerActionCreators();
//# sourceMappingURL=ChatServerActionCreators.js.map