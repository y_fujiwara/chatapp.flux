﻿import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import ReceiveRawMessages from '../messages/ReceiveRawMessages';
import ReceiveRawCreatedMessage from '../messages/ReceiveRawCreatedMessage';
import RawMessage from '../models/RawMessage';

class ChatServerActionCreators {
    receiveAll(rawMessagesp: RawMessage[]) {
        ChatAppDispatcher.dispatch(new ReceiveRawMessages(rawMessagesp));
    }
    receiveCreatedMessage(createdMessage: RawMessage) {
        ChatAppDispatcher.dispatch(new ReceiveRawCreatedMessage(createdMessage));
    }
}

export default new ChatServerActionCreators();