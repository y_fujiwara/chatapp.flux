﻿import ChatAppDispathcer from '../dispatcher/ChatAppDispatcher';
import ClickThread from '../messages/ClickThread';

class ChatThreadActionCreators {
    clickThread(threadID: string) {
        ChatAppDispathcer.dispatch(new ClickThread(threadID));
    }
}

export default new ChatThreadActionCreators();