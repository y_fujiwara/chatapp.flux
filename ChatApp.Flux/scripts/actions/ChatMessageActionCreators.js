"use strict";
var ChatAppDispatcher_1 = require('../dispatcher/ChatAppDispatcher');
var CreateMessage_1 = require('../messages/CreateMessage');
var ChatWebAPIUtils_1 = require('../utils/ChatWebAPIUtils');
var ChatMessageUtils_1 = require('../utils/ChatMessageUtils');
var ChatMessageActionCreators = (function () {
    function ChatMessageActionCreators() {
    }
    ChatMessageActionCreators.prototype.createMessage = function (text, currentThreadID) {
        ChatAppDispatcher_1.default.dispatch(new CreateMessage_1.default(text, currentThreadID));
        var message = ChatMessageUtils_1.default.getCreatedMessageData(text, currentThreadID);
        ChatWebAPIUtils_1.default.createMessage(message, null);
    };
    return ChatMessageActionCreators;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new ChatMessageActionCreators();
//# sourceMappingURL=ChatMessageActionCreators.js.map