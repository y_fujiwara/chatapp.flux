﻿/// <reference path="../../typings/react/react.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from 'react';
import Message from '../models/Message';


interface MessageListItemProps extends React.Props<{}> {
    message: Message;
}

export default class MessageListItem extends React.Component<MessageListItemProps, {}> {
    render() {
        var message = this.props.message;
        return (
            <li className='message-list-item'>
                <h5> className='message-author-name'>{message.authorName}</h5>
                <div className='message-time'>
                    {message.date.toLocaleDateString() }
                </div>
                <div className='message-text'>{message.text}</div>
            </li>
        )
    }
}