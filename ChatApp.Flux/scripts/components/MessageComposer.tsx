﻿/// <reference path="../../typings/react/react.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from 'react';
import ChatMessageActionCreators from '../actions/ChatMessageActionCreators';

const ENTER_KEY_CODE = 13;

interface MessageComposerProps extends React.Props<{}> {
    threadID: string;
}

interface MessageComposerState {
    text: string;
}

export default class MessageComposer extends React.Component<MessageComposerProps, MessageComposerState> {
    constructor(props: MessageComposerProps) {
        super(props); this.state = { text: '' };
    }

    private onChange(event: React.SyntheticEvent) {
        this.setState({ text: (event.target as HTMLInputElement).value });
    }

    private onKeyDown(event: React.KeyboardEvent) {
        if (event.keyCode === ENTER_KEY_CODE) {
            event.preventDefault();
            var text = this.state.text.trim();
            if (text) {
                ChatMessageActionCreators.createMessage(text, this.props.threadID);
                this.setState({ text: '' });
            }
        }
    }

    render() {
        return (
            <textarea className = 'message-composer'
                name ='message'
                value = {this.state.text}
                onChange = {this.onChange.bind(this) }
                onKeyDown ={this.onKeyDown.bind(this) } />
        );
    }
}