﻿/// <reference path="../../typings/react/react.d.ts" />
/// <reference path="../../typings/react/react-dom.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import MessageComposer from './MessageComposer';
import MessageListItem from './MessageListItem';
import MessageStore from '../stores/MessageStore';
import ThreadStore from '../stores/ThreadStore';
import Message from '../models/Message';
import Thread from '../models/Thread';

interface MessageSectionState {
    messages: Message[];
    thread: Thread;
}

export default class MessageSection extends React.Component<{}, MessageSectionState> {
    private messageStoreSubscription: { remove: Function };
    private threadStoreSubscription: { remove: Function };

    constructor(props: {}) {
        super(props);
        this.state = this.getStateFromStores();
    }

    private getStateFromStores(): MessageSectionState {
        return {
            messages: MessageStore.getAllForCurrentThread(),
            thread: ThreadStore.getCurrent()
        };
    }

    private getMessageListItem(message: Message) {
        return (<MessageListItem key ={message.id} message ={message} />);
    }

    private scrollToBottom() {
        var ul = ReactDOM.findDOMNode(this.refs['messageList']);
        ul.scrollTop = ul.scrollHeight;
    }

    private onChange() {
        this.setState(this.getStateFromStores());
    }

    componentDidMount() {
        this.messageStoreSubscription = MessageStore.addListener(this.onChange.bind(this));
        this.threadStoreSubscription = ThreadStore.addListener(this.onChange.bind(this));
    }

    componentWillUnmount() {
        this.messageStoreSubscription.remove();
        this.threadStoreSubscription.remove();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        var messageListItems = this.state.messages.map(this.getMessageListItem);
        return (
            <div className = 'message-section'>
                <h3 className = 'message-thread-heading'>{this.state.thread.name}</h3>
                <ul className = 'message-list' ref = 'messageList'>{messageListItems}</ul>
                <MessageComposer threadID = {this.state.thread.id} />
            </div>
        );
    }
}