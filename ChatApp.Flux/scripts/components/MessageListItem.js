/// <reference path="../../typings/react/react.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var MessageListItem = (function (_super) {
    __extends(MessageListItem, _super);
    function MessageListItem() {
        _super.apply(this, arguments);
    }
    MessageListItem.prototype.render = function () {
        var message = this.props.message;
        return (React.createElement("li", {className: 'message-list-item'}, React.createElement("h5", null, " className='message-author-name'>", message.authorName), React.createElement("div", {className: 'message-time'}, message.date.toLocaleDateString()), React.createElement("div", {className: 'message-text'}, message.text)));
    };
    return MessageListItem;
}(React.Component));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MessageListItem;
//# sourceMappingURL=MessageListItem.js.map