/// <reference path="../../typings/react/react.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var ChatMessageActionCreators_1 = require('../actions/ChatMessageActionCreators');
var ENTER_KEY_CODE = 13;
var MessageComposer = (function (_super) {
    __extends(MessageComposer, _super);
    function MessageComposer(props) {
        _super.call(this, props);
        this.state = { text: '' };
    }
    MessageComposer.prototype.onChange = function (event) {
        this.setState({ text: event.target.value });
    };
    MessageComposer.prototype.onKeyDown = function (event) {
        if (event.keyCode === ENTER_KEY_CODE) {
            event.preventDefault();
            var text = this.state.text.trim();
            if (text) {
                ChatMessageActionCreators_1.default.createMessage(text, this.props.threadID);
                this.setState({ text: '' });
            }
        }
    };
    MessageComposer.prototype.render = function () {
        return (React.createElement("textarea", {className: 'message-composer', name: 'message', value: this.state.text, onChange: this.onChange.bind(this), onKeyDown: this.onKeyDown.bind(this)}));
    };
    return MessageComposer;
}(React.Component));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MessageComposer;
//# sourceMappingURL=MessageComposer.js.map