"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../typings/react/react.d.ts" />
/// <reference path="../../typings/react/react-dom.d.ts" />
// A '.tsx' file enables JSX support in the TypeScript compiler,
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
var React = require('react');
var ReactDOM = require('react-dom');
var MessageComposer_1 = require('./MessageComposer');
var MessageListItem_1 = require('./MessageListItem');
var MessageStore_1 = require('../stores/MessageStore');
var ThreadStore_1 = require('../stores/ThreadStore');
var MessageSection = (function (_super) {
    __extends(MessageSection, _super);
    function MessageSection(props) {
        _super.call(this, props);
        this.state = this.getStateFromStores();
    }
    MessageSection.prototype.getStateFromStores = function () {
        return {
            messages: MessageStore_1.default.getAllForCurrentThread(),
            thread: ThreadStore_1.default.getCurrent()
        };
    };
    MessageSection.prototype.getMessageListItem = function (message) {
        return (React.createElement(MessageListItem_1.default, {key: message.id, message: message}));
    };
    MessageSection.prototype.scrollToBottom = function () {
        var ul = ReactDOM.findDOMNode(this.refs['messageList']);
        ul.scrollTop = ul.scrollHeight;
    };
    MessageSection.prototype.onChange = function () {
        this.setState(this.getStateFromStores());
    };
    MessageSection.prototype.componentDidMount = function () {
        this.messageStoreSubscription = MessageStore_1.default.addListener(this.onChange.bind(this));
        this.threadStoreSubscription = ThreadStore_1.default.addListener(this.onChange.bind(this));
    };
    MessageSection.prototype.componentWillUnmount = function () {
        this.messageStoreSubscription.remove();
        this.threadStoreSubscription.remove();
    };
    MessageSection.prototype.componentDidUpdate = function () {
        this.scrollToBottom();
    };
    MessageSection.prototype.render = function () {
        var messageListItems = this.state.messages.map(this.getMessageListItem);
        return (React.createElement("div", {className: 'message-section'}, React.createElement("h3", {className: 'message-thread-heading'}, this.state.thread.name), React.createElement("ul", {className: 'message-list', ref: 'messageList'}, messageListItems), React.createElement(MessageComposer_1.default, {threadID: this.state.thread.id})));
    };
    return MessageSection;
}(React.Component));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MessageSection;
//# sourceMappingURL=MessageSection.js.map